from flask import Flask
from flask_restful import Api, Resource, reqparse
from mailing_logic import send_emails
import json
import re

app = Flask(__name__)
api = Api(app)

# Проверка и парсинг полученных запросов на наличие необходимых аттрибутов

client_put_args = reqparse.RequestParser()
client_put_args.add_argument("client_id", type=int, help='client_id is a required field in INT format', required=True)
client_put_args.add_argument("number", type=str, help='number is a required field', required=True)
client_put_args.add_argument("operator_code", type=str, help='operator_code is a required field', required=True)
client_put_args.add_argument("tag", type=str)
client_put_args.add_argument("timezone", type=str, help='timezone is a required field', required=True)


client_get_args = reqparse.RequestParser()
client_get_args.add_argument('client_id', type=int, help='client_id is a required field in INT format', required=True)

client_patch_args = reqparse.RequestParser()
client_patch_args.add_argument("client_id", type=int, help='client_id is a required field in INT format', required=True)
client_patch_args.add_argument("number", type=str)
client_patch_args.add_argument("operator_code", type=str)
client_patch_args.add_argument("tag", type=str)
client_patch_args.add_argument("timezone", type=str)

mailing_put_args = reqparse.RequestParser()
mailing_put_args.add_argument("mailing_id", type=int, help='mailing_id is a required field', required=True)
mailing_put_args.add_argument("launch_time", type=str, help='launch_time is a required field', required=True)
mailing_put_args.add_argument("message_text", type=str, help='message_text is a required field', required=True)
mailing_put_args.add_argument("operator_code", type=str)
mailing_put_args.add_argument("tag", type=str)
mailing_put_args.add_argument("end_time", type=str, help='end_time is a required field', required=True)

mailing_get_args = reqparse.RequestParser()
mailing_get_args.add_argument('mailing_id', type=int, help='mailing_id is a required field in INT format', required=True)

mailing_patch_args = reqparse.RequestParser()
mailing_patch_args.add_argument("mailing_id", type=int)
mailing_patch_args.add_argument("launch_time", type=str)
mailing_patch_args.add_argument("message_text", type=str)
mailing_patch_args.add_argument("operator_code", type=str)
mailing_patch_args.add_argument("tag", type=str)
mailing_patch_args.add_argument("end_time", type=str)


class Client(Resource):

    def get(self):
        args = client_get_args.parse_args()
        with open("storage.json", "r") as f:
            data = json.load(f)
        for client in data['clients']:
            if client['client_id'] == args['client_id']:
                return client
        return {'data': f'no client with such client_id ({args["client_id"]})'}, 401

    def post(self):
        args = client_put_args.parse_args()
        with open("storage.json", "r") as f:
            data = json.load(f)
        if not re.fullmatch(r"7\d{10}", args['number']):
            return {'data': 'the number format needs to be 7XXXXXXXXXX'}
        for client in data['clients']:
            if client['client_id'] == args['client_id']:
                return {'data': f'client with this client_id ({args["client_id"]}) already exists'}, 401
        data['clients'].append({"client_id": args['client_id'], "number": args['number'], "operator_code": args['operator_code'], "tag": args['tag'], "timezone": args['timezone']})
        with open("storage.json", "w") as f:
            json.dump(data, f)
        return {'data': 'client created'}, 200

    def delete(self):
        args = client_get_args.parse_args()
        with open('storage.json', 'r') as f:
            data = json.load(f)
        for client in data['clients']:
            if client['client_id'] == args['client_id']:
                data['clients'] = list(filter(lambda x: x['client_id'] != args['client_id'], data['clients']))
                with open('storage.json', 'w') as f:
                    json.dump(data, f)
                return {'data': 'client deleted'}
        return {'data': f'no client with such client_id ({args["client_id"]})'}

    def patch(self):
        args = client_patch_args.parse_args()
        with open("storage.json", "r") as f:
            data = json.load(f)

        for i, client in enumerate(data['clients']):
            if client['client_id'] == args['client_id']:
                print(args)
                client['number'] = args['number'] if ('number' in args and args['number'] is not None and re.fullmatch(r"7\d{10}", args['number'])) else client['number']
                client['operator_code'] = args['operator_code'] if ('operator_code' in args and args['operator_code'] is not None) else client['operator_code']
                client['tag'] = args['tag'] if ('tag' in args and args['tag'] is not None) else client['tag']
                client['timezone'] = args['timezone'] if ('timezone' in args and args['timezone'] is not None) else client['timezone']
                data['clients'][i] = client
                print('CLIENT ', client)
                with open("storage.json", "w") as f:
                    json.dump(data, f)

                return {'data': 'client updated'}, 200

        return {'data': f'no client with such client_id ({args["client_id"]})'}, 401





class Mailing(Resource):

    def post(self):
        args= mailing_put_args.parse_args()
        with open("storage.json", "r") as f:
            data = json.load(f)
        for mailing in data['mailings']:
            if mailing['mailing_id'] == args['mailing_id']:
                return {'data': f'mailing with this mailing_id ({args["mailing_id"]}) already exists'}, 401
        data['mailings'].append({"mailing_id": args['mailing_id'], "launch_time": args['launch_time'], "message_text": args['message_text'], "operator_code": args['operator_code'], "tag": args['tag'], "end_time": args['end_time']})
        with open("storage.json", "w") as f:
            json.dump(data, f)
        send_emails(args['mailing_id'])
        return {'data': 'mailing created'}, 200

    def get(self):
        args = mailing_get_args.parse_args()
        with open("storage.json", "r") as f:
            data = json.load(f)
        for mailing in data['mailings']:
            if mailing['mailing_id'] == args['mailing_id']:
                return mailing
        return {'data': f'no mailing with such mailing_id ({args["mailing_id"]})'}, 401

    def delete(self):
        args = mailing_get_args.parse_args()
        with open('storage.json', 'r') as f:
            data = json.load(f)
        for mailing in data['mailings']:
            if mailing['mailing_id'] == args['mailing_id']:
                data['mailings'] = list(filter(lambda x: x['mailing_id'] != args['mailing_id'], data['mailings']))
                with open('storage.json', 'w') as f:
                    json.dump(data, f)
                return {'data': 'mailing deleted'}
        return {'data': f'no mailing with such mailing_id ({args["mailing_id"]})'}

    def patch(self):
        args = mailing_patch_args.parse_args()
        with open("storage.json", "r") as f:
            data = json.load(f)
        for i, mailing in enumerate(data['mailings']):
            if mailing['mailing_id'] == args['mailing_id']:
                print(args)
                mailing['launch_time'] = args['launch_time'] if ('launch_time' in args and args['launch_time'] is not None) else mailing['launch_time']
                mailing['message_text'] = args['message_text'] if ('message_text' in args and args['message_text'] is not None) else mailing['message_text']
                mailing['operator_code'] = args['operator_code'] if ('operator_code' in args and args['operator_code'] is not None) else mailing['operator_code']
                mailing['tag'] = args['tag'] if ('tag' in args and args['tag'] is not None) else mailing['tag']
                mailing['end_time'] = args['end_time'] if ('end_time' in args and args['end_time'] is not None) else mailing['end_time']
                data['mailings'][i] = mailing
                print('mailing ', mailing)
                with open("storage.json", "w") as f:
                    json.dump(data, f)

                return {'data': 'mailing updated'}, 200

        return {'data': f'no mailing with such client_id ({args["mailing_id"]})'}, 401


#api.add_resource(CreateClient, '/add_client/<int:client_id>/<string:number>/<string:operator_code>/<string:tag>/<string:timezone>')
api.add_resource(Client, '/client/')

api.add_resource(Mailing, '/mailing/')


if __name__ == "__main__":
    app.run(port=8000, debug=True)