import datetime
import requests
import json
import sched
#import sys, os
#from crontab import CronTab

def send_request(msg):
    token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE3MDI2NDg4NDEsImlzcyI6ImZhYnJpcXVlIiwibmFtZSI6ImFsdDBzcGFjZSJ9.s5eQMQFggYqRYE5bfcA6d8dfVwR-oSOt40KgQ25ZYCE'

    headers = {
        "Authorization": f"Bearer {token}",
        "Content-Type": "application/json"
    }
    host = 'https://probe.fbrq.cloud/v1/send/'
    resp = requests.post(host+str(msg['id']), headers=headers, json=msg)
    return resp


def send_email(mailing):
    with open("storage.json", "r") as f:
        data = json.load(f)
    clients = data['clients']
    with open("messages_log.json", "r") as f:
        data = json.load(f)
    for client in clients:
        if client['tag'] == mailing['tag'] or client['operator_code'] == mailing['operator_code']:
            msg = {
                'id': data['id']+1,
                'phone': client['number'],
                'text': mailing['message_text']
            }
            while True:
                resp = send_request(msg)
                print(resp.text)
                log = {'message_id': msg['id'],
                       'time': str(datetime.datetime.now()),
                       'client_id': client['client_id'],
                       'mailing_id': mailing['mailing_id'],
                       'status': resp.status_code
                       }
                with open("messages_log.json", "r") as f:
                    data = json.load(f)
                data['messages'].append(log)
                data['id'] += 1
                with open("messages_log.json", "w") as f:
                    json.dump(data, f)
                if resp.status_code == 200:
                    break


def send_emails(mailing_id):
    with open("storage.json", "r") as f:
        data = json.load(f)
    mailings = data['mailings']
    for mailing in mailings:
        if mailing['mailing_id'] == mailing_id:
            cur_date = datetime.datetime.now()
            if datetime.datetime.strptime(mailing['launch_time'], "%d-%m-%Y %H:%M") < cur_date < datetime.datetime.strptime(mailing['end_time'], "%d-%m-%Y %H:%M"):
                send_email(mailing)
            else:
                if datetime.datetime.strptime(mailing['launch_time'], "%d-%m-%Y %H:%M") > cur_date:
                    scheduled_time = datetime.datetime.strptime(mailing['launch_time'], "%d-%m-%Y %H:%M")
                    scheduler = sched.scheduler()
                    delay = scheduled_time - datetime.now()
                    scheduler.enter(delay.total_seconds(), 1, send_email, argument=(mailing,))
                    scheduler.run()
                    '''
                    cron = CronTab(user='root')
                    python_executable = os.path.dirname(os.path.realpath(sys.executable))
                    command = f"{python_executable} -c 'from mailing_logic import send_email; send_email({mailing})'"
                    job = cron.new(command=command)
                    job.setall(scheduled_time.minute, scheduled_time.hour, scheduled_time.day, scheduled_time.month, "*")
                    cron.write()
                    '''
